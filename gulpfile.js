// Основний модуль
import gulp from "gulp"
// Імпорт путей
import { path } from "./gulp/config/path.js";
//Імпорт общих плагінов
import { plugins } from "./gulp/config/plugins.js";

//Передаєм значення в  глобальну перемену 
global.app = {
    path: path,
    gulp: gulp,
    plugins: plugins,
}

// Іипорт задач
import { copy } from "./gulp/tasks/copy.js";
import { reset } from "./gulp/tasks/reset.js";
import { html } from "./gulp/tasks/html.js";
import { server } from "./gulp/tasks/server.js";
import { scss } from "./gulp/tasks/scss.js";

 
// Наблюдаєм за змінами у файлах
function watcher(){
    gulp.watch(path.watch.files, copy);
    gulp.watch(path.watch.html, html);
    gulp.watch(path.watch.scss, scss);
    
}

// Основні задачі
const mainTasks = gulp.parallel(copy, html, scss);

// Побудова сценаріїв виконаних задач
const dev = gulp.series(reset, mainTasks, gulp.parallel(watcher, server));


//Виконуєм сценарій по замовчуванню
gulp.task('default', dev);

