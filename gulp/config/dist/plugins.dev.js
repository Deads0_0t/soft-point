"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.plugins = void 0;

var _gulpReplace = _interopRequireDefault(require("gulp-replace"));

var _gulpPlumber = _interopRequireDefault(require("gulp-plumber"));

var _gulpNotify = _interopRequireDefault(require("gulp-notify"));

var _browserSync = _interopRequireDefault(require("browser-sync"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// import fileInclude from "gulp-file-include";  
//Поиск и замена
// Обробка ошибок
// Повідомлення (підказки)
//Локальний сервер
//Експортируєм обєкт
var plugins = {
  replace: _gulpReplace["default"],
  plumber: _gulpPlumber["default"],
  notify: _gulpNotify["default"],
  browsersync: _browserSync["default"]
};
exports.plugins = plugins;