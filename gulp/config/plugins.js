// import fileInclude from "gulp-file-include";  
import replace from "gulp-replace"; //Поиск и замена
import plumber from "gulp-plumber"; // Обробка ошибок
import notify from "gulp-notify"; // Повідомлення (підказки)
import browsersync from "browser-sync"; //Локальний сервер

//Експортируєм обєкт
export const plugins = {
    replace: replace,
    plumber: plumber,
    notify: notify,
    browsersync: browsersync,
}